-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema MyShop
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `MyShop` ;

-- -----------------------------------------------------
-- Schema MyShop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `MyShop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `MyShop` ;

-- -----------------------------------------------------
-- Table `MyShop`.`Usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MyShop`.`Usuario` ;

CREATE TABLE IF NOT EXISTS `MyShop`.`Usuario` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `username` VARCHAR(45) NOT NULL COMMENT '',
  `password` VARCHAR(128) NOT NULL COMMENT '',
  `email` VARCHAR(128) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `email_UNIQUE` (`email` ASC)  COMMENT '',
  UNIQUE INDEX `username_UNIQUE` (`username` ASC)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MyShop`.`amigos`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MyShop`.`amigos` ;

CREATE TABLE IF NOT EXISTS `MyShop`.`amigos` (
  `idUsuario` INT NOT NULL COMMENT '',
  `idAmigo` INT NOT NULL COMMENT '',
  PRIMARY KEY (`idUsuario`, `idAmigo`)  COMMENT '',
  INDEX `fk_amigos_Usuario1_idx` (`idAmigo` ASC)  COMMENT '',
  CONSTRAINT `fk_amigos_Usuario`
    FOREIGN KEY (`idUsuario`)
    REFERENCES `MyShop`.`Usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_amigos_Usuario1`
    FOREIGN KEY (`idAmigo`)
    REFERENCES `MyShop`.`Usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `MyShop`.`Tienda`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `MyShop`.`Tienda` ;

CREATE TABLE IF NOT EXISTS `MyShop`.`Tienda` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `owner` INT NULL COMMENT '',
  `titulo` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `owner_UNIQUE` (`owner` ASC)  COMMENT '',
  CONSTRAINT `fk_Tienda_Usuario1`
    FOREIGN KEY (`owner`)
    REFERENCES `MyShop`.`Usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `MyShop`.`Usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `MyShop`;
INSERT INTO `MyShop`.`Usuario` (`id`, `username`, `password`, `email`) VALUES (DEFAULT, 'pabhoz', '1234', 'pabhoz@gmail.com');

COMMIT;

