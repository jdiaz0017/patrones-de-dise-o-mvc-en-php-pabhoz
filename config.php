<?php

    define('URL', 'http://localhost/Youtube/videos/MyShop/');
    define('LIBS','libs/');
    define('MODELS','./models/');
    define('BS','./bussinesLogic/');
    define('MODULE','./views/modules/');
    
    define('_DB_TYPE', 'mysql');
    define('_DB_HOST' , 'localhost');
    define('_DB_USER' , 'root' );
    define('_DB_PASS' , '' );
    define('_DB_NAME' , 'myshop');
    
    define('HASH_ALGO' , 'sha512');
    define('HASH_KEY' , 'my_key');
    define('HASH_SECRET' , 'my_secret');
    define('SECRET_WORD' , 'so_secret');